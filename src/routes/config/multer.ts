import { Options, diskStorage } from 'multer';
import { resolve } from 'path';
import { randomBytes } from 'crypto';
import { request } from 'http';

export const multerConfig = {
  dest: resolve(__dirname, '..', '..', 'tmp'),
  storage: diskStorage({
    destination: (request, file, callback) => {
      callback(null, resolve(__dirname, '..', '..', 'tmp'));
    },
    filename: (request, file, callback) => {
      randomBytes(6, (error, hash) => {
        if (error) {
          callback(error, file.filename);
        }
        const filename = `${hash.toString('hex')}.xlsx`;
        callback(null, filename);
      });
    },
  }),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
  fileFilter: (request, file, callback) => {
    const formats = [
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];

    if (formats.includes(file.mimetype)) {
      callback(null, true);
    } else {
      callback(new Error('Formato de arquivo não suportado'));
    }
  },
} as Options;
