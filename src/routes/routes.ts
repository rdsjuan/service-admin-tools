import { Router, Request, Response } from 'express';
import { multerConfig } from './config/multer';
import multer from 'multer';
import excel2json from '../libs/excel2json';
const routes = Router();

routes.get('/', (req: Request, res: Response) => {
  return res.json({ message: 'Hello World!' });
});

routes.post('/upload', multer(multerConfig).single('file'), (req: Request, res: Response) => {
  const teste = excel2json(req.file?.filename || '');
  console.log(req.file);
  return res.json(teste);
});

export default routes;
