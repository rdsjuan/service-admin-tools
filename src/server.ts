import express, { NextFunction, Request, Response } from 'express';
import path from 'path';
import dotenv from 'dotenv';
import routes from './routes/routes';

dotenv.config();

const server = express();

server.use(express.json());
server.use(routes);
server.use(express.static(path.join(__dirname, '../public')));
server.use(express.urlencoded({ extended: true }));

server.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  res.status(500).json({
    error: err.message,
  });
});

server.listen(process.env.PORT);
