const xlsx = require('xlsx');
const fs = require('fs');

//! read data from excel and store in json file
const xls2json = (filename: string) => {
  try {
    //! read excel file
    const wb = xlsx.readFile(`./src/tmp/${filename}`, { cellDates: true });
    // console.log(wb.SheetNames);
    const sheetNames = wb.SheetNames;
    const totalSheets = sheetNames.length;
    const data = [];
    for (let i = 0; i < totalSheets; i++) {
      const tempData = xlsx.utils.sheet_to_json(wb.Sheets[sheetNames[i]]);
      tempData.shift();
      data.push(...tempData);
    }

    //! write json data to file
    fs.writeFileSync('./datajson.json', JSON.stringify(data, null, 2));
    return data;
  } catch (error) {
    return { error: 'teste' };
  }
};

export default xls2json;
